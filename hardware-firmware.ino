#include <AFMotor.h> //the motor sheild library
#include <NewPing.h> //the PING library
#include <Servo.h>   //the servo library

//-----------Constant declarations----------------------------------------------------------
#define TRIG_PIN A4 // sets Analog Pin 4 as trigger Pin of PING
#define ECHO_PIN A5 // sets Analog Pin 5 as echo Pin of PING
#define SERVO_PIN 9
#define SONAR_MAX_DISTANCE 200 // sets max obstacleDistance of PING sensor
#define MOTOR_MAX_SPEED 120    // sets max speed of motors
#define SAFE_DISTANCE 25
#define SPEED_DELTA 50 // delta value for speed increement/decreement
#define LEFT 180
#define RIGHT 0
#define FRONT 90

//-----------Function declarations-----------------------------------------------------------
void moveForward(void);
void moveBackward(void);
void turnLeft(void);
void turnRight(void);
void stopCar(void);
void accelerateCar(void);
int getObstacleDistance(int direction);
int readPing(void);
void initializeComponents(void);

//-----------Object declarations-------------------------------------------------------------
NewPing sonar(TRIG_PIN, ECHO_PIN, SONAR_MAX_DISTANCE);
AF_DCMotor leftFrontWheel(1, MOTOR12_1KHZ);
AF_DCMotor rightFrontWheel(2, MOTORl2_1KHZ);
AF_DCMotor leftRearWheel(3, MOTOR34_1KHZ);
AF_DCMotor rightRearWheel(4, MOTOR34_1KHZ);

//-----------Variable declarations----------------------------------------------------------
Servo head;
int currentSpeed = 0;

void setup()
{
  initializeComponents();
}

void loop()
{
  if (getObstacleDistance(FRONT) < SAFE_DISTANCE)
  {
    stopCar();
    moveBackward();
    if (getObstacleDistance(LEFT) < getObstacleDistance(RIGHT))
    {
      turnRight();
    }
    else
    {
      turnLeft();
    }
  }
  moveForward();
  accelerateCar();
}

void initializeComponents(void)
{
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  head.attach(SERVO_PIN);
  head.write(FRONT);
  delay(2000);
}

void moveForward(void)
{
  leftFrontWheel.run(FORWARD);
  rightFrontWheel.run(FORWARD);
  leftRearWheel.run(FORWARD);
  rightRearWheel.run(FORWARD);
  delay(750);
}

void moveBackward(void)
{
  leftFrontWheel.run(BACKWARD);
  rightFrontWheel.run(BACKWARD);
  leftRearWheel.run(BACKWARD);
  rightRearWheel.run(BACKWARD);
  delay(750);
}

void turnLeft(void)
{
  leftFrontWheel.run(BACKWARD);
  rightFrontWheel.run(BACKWARD);
  leftRearWheel.run(FORWARD);
  rightRearWheel.run(FORWARD);
  delay(750);
}

void turnRight(void)
{
  leftFrontWheel.run(FORWARD);
  rightFrontWheel.run(FORWARD);
  leftRearWheel.run(BACKWARD);
  rightRearWheel.run(BACKWARD);
  delay(750);
}

int getObstacleDistance(int direction)
{
  int distance = 0;

  head.write(direction);
  delay(1000);
  distance += readPing();
  delay(100);
  distance += readPing();
  delay(100);
  head.write(90);
  return distance / 2; // taking average value of data from SONAR
}

int readPing(void)
{
  delay(70);
  int cm = sonar.ping_cm();
  Serial.begin(9600);
  Serial.println(cm);
  if (cm == 0)
  {
    cm = 250;
  }
  return cm;
}

void stopCar(void)
{
  leftFrontWheel.run(RELEASE);
  rightFrontWheel.run(RELEASE);
  leftRearWheel.run(RELEASE);
  rightRearWheel.run(RELEASE);
}

void accelerateCar(void)
{
  for (int currentSpeed = 0; currentSpeed < MOTOR_MAX_SPEED; currentSpeed += SPEED_DELTA) // slowly bring the speed up to avoid loading down the batteries too quickly
  {
    leftFrontWheel.setSpeed(currentSpeed);
    rightFrontWheel.setSpeed(currentSpeed);
    leftRearWheel.setSpeed(currentSpeed);
    rightRearWheel.setSpeed(currentSpeed);
    delay(5);
  }
}